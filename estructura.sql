create table sis_my_ciudad(
    id int auto_increment primary key,
    nombre varchar(200),
    usar_en_filtro char(1),
    ambito char(1),
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
)COLLATE='utf8_general_ci' ENGINE=InnoDB;

create table sis_my_plan(
    id int auto_increment primary key,
    id_origen int,
    id_destino int,
    tarifa decimal(12,2),
    link varchar(300),
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
)COLLATE='utf8_general_ci' ENGINE=InnoDB;