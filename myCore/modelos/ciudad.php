<?php

class Ciudad extends myEloquent {    
    protected $table = 'my_ciudad';
    
    protected $fillable = array('id', 'nombre', 'ambito', 'usar_en_filtro');
    
    public function planes(){
        return $this->hasMany('Plan', 'id_origen');
    }  
    
    public function planesDest(){
        return $this->hasMany('Plan', 'id_destino');
    }
}
