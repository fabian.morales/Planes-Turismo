<?php

class Plan extends myEloquent {    
    protected $table = 'my_plan';
    
    protected $fillable = array('id', 'id_origen', 'id_destino', 'tarifa', 'link');
    
    public function origen(){
        return $this->belongsTo('Ciudad', 'id_origen');
    }
    
    public function destino(){
        return $this->belongsTo('Ciudad', 'id_destino');
    }
}
