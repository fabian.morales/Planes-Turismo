<?php
class tarifasController extends myController{
    function index(){
        $doc = myApp::getDocumento();
        $doc->addEstilo(JUri::root()."myCore/css/my.css");
        
        $ciudad = null;
        $id = myApp::getRequest()->getVar("id");
        $ciudades = Ciudad::where("usar_en_filtro", "S")->orderBy("nombre")->get();
        
        if (!empty($id)){
            $ciudad = Ciudad::find($id);
        }
        
        if (!sizeof($ciudad)){
            $ciudad = $ciudades[0];
        }
                
        $nacionales = Plan::with("destino")->where("id_origen", $ciudad->id)->whereHas("destino", function($q) {
            $q->where("ambito", "N");
        })->get();
        
        $internacionales = Plan::with("destino")->where("id_origen", $ciudad->id)->whereHas("destino", function($q) {
            $q->where("ambito", "I");
        })->get();
        
        return myView::render("tarifas.index", ["ciudades" => $ciudades, "nacionales" => $nacionales, "internacionales" => $internacionales, "ciudad" => $ciudad]);
    }
    
    public function mostrarDestinosNal($ciudad = null){
        if (!sizeof($ciudad)){
            $id = myApp::getRequest()->getVar("id");
            $ciudad = Ciudad::find($id);
        }
        
        $nacionales = Plan::with("destino")->where("id_origen", $ciudad->id)->whereHas("destino", function($q) {
            $q->where("ambito", "N");
        })->get();

        return myView::render("tarifas.destinos", ["ciudad" => $ciudad, "destinos" => $nacionales]);
    }
    
    public function mostrarDestinosInt($ciudad = null){
        if (!sizeof($ciudad)){
            $id = myApp::getRequest()->getVar("id");
            $ciudad = Ciudad::find($id);
        }
        
        $internacionales = Plan::with("destino")->where("id_origen", $ciudad->id)->whereHas("destino", function($q) {
            $q->where("ambito", "I");
        })->get();
               
        return myView::render("tarifas.destinos", ["ciudad" => $ciudad, "destinos" => $internacionales]);
    }
}