<?php
    
use \Illuminate\Database\Capsule\Manager as Capsule;

class adminCiudadController extends myAdminController{
    public function __construct() {
        $doc = myApp::getDocumento();
        $doc->addScript(JUri::root()."media/jui/js/jquery.min.js");
        $doc->addEstilo(JUri::root()."media/jui/css/bootstrap.css");
        $doc->incluirLibJs("fancybox", array("fancybox"));
        $doc->addScript(JUri::root()."myCore/js/admin.js");
    }
    
    public function index(){
        JToolbarHelper::title('Gestión de ciudades');
        $ciudades = Ciudad::paginate(20);
        return myView::render("admin.ciudad.index", ["ciudades" => $ciudades]);
    }
    
    public function crearCiudad(){
        return $this->formCiudad(new Ciudad());
    }
    
    public function editarCiudad(){
        $id = myApp::getRequest()->getVar("id");
        $ciudad = Ciudad::find($id);

        if (!sizeof($ciudad)){
            myApp::redirect("index.php?option=com_my_component&controller=adminCiudad", "Ciudad no encontrada");
        }

        return $this->formCiudad($ciudad);
    }
    
    public function formCiudad($ciudad){
        JToolbarHelper::title('Gestión de ciudades');
        return myView::render("admin.ciudad.form", ["ciudad" => $ciudad]);
    }
    
    public function guardarCiudad(){
        $request = myApp::getRequest();
        $id = myApp::getRequest()->getVar("id");
        $ciudad = Ciudad::find($id);
        
        if (!sizeof($ciudad)){
            $ciudad = new Ciudad();
        }
        
        $ciudad->fill($request->all());
        
        if (empty($ciudad->usar_en_filtro)){
            $ciudad->usar_en_filtro = 'N';
        }
        
        if ($ciudad->save()){
            myApp::redirect("index.php?option=com_my_component&controller=adminCiudad", "Ciudad guardada");
        }
        else{
            myApp::redirect("index.php?option=com_my_component&controller=adminCiudad", "No se pudo guardar la ciudad");
        }
    }
    
    public function borrarCiudad(){
        $id = myApp::getRequest()->getVar("id");
        $ciudad = Ciudad::find($id);
        
        if (!sizeof($ciudad)){
            myApp::redirect("index.php?option=com_my_component&controller=adminCiudad", "Ciudad no encontrada");
        }
        
        if ($ciudad->delete()){
            myApp::redirect("index.php?option=com_my_component&controller=adminCiudad", "Ciudad borrada");
        }
        else{
            myApp::redirect("index.php?option=com_my_component&controller=adminCiudad", "No se pudo borrar la ciudad");
        }
    }
    
    public function verPlanes(){
        $id = myApp::getRequest()->getVar("id");
        $ciudad = Ciudad::find($id);
        $destinos = Ciudad::with(array("planesDest" => function($q) use ($id) {
            $q->where("id_origen", $id);
        }))->get();
        return myView::render("admin.ciudad.form_planes", ["ciudad" => $ciudad, "destinos" => $destinos]);
    }
    
    public function verLinks(){
        $id = myApp::getRequest()->getVar("id");
        $ciudad = Ciudad::find($id);
        $destinos = Ciudad::with(array("planesDest" => function($q) use ($id) {
            $q->where("id_origen", $id);
        }))->get();
        return myView::render("admin.ciudad.form_links", ["ciudad" => $ciudad, "destinos" => $destinos]);
    }
    
    public function guardarPlanes(){
        $idCiudad = myApp::getRequest()->getVar("id");
        $tarifas = myApp::getRequest()->getVar("tarifas", [], "ARRAY");
        
        foreach ($tarifas as $i => $t){
            $plan = Plan::where("id_origen", $idCiudad)->where("id_destino", $i)->first();
            if (!sizeof($plan)){
                $plan = new Plan();
                $plan->id_origen = $idCiudad;
                $plan->id_destino = $i;
                $plan->link = '';
            }
            
            $plan->tarifa = (float)$t;
            $plan->save();
        }
        
        myApp::redirect("index.php?option=com_my_component&controller=adminCiudad", "Tarifas guardadas");
    }
    
    public function guardarLinks(){
        $idCiudad = myApp::getRequest()->getVar("id");
        $links = myApp::getRequest()->getVar("links", [], "ARRAY");
        
        foreach ($links as $i => $l){
            $plan = Plan::where("id_origen", $idCiudad)->where("id_destino", $i)->first();
            if (!sizeof($plan)){
                $plan = new Plan();
                $plan->id_origen = $idCiudad;
                $plan->id_destino = $i;
                $plan->tarifa = 0;
            }
            
            $plan->link = $l;
            $plan->save();
        }
        
        myApp::redirect("index.php?option=com_my_component&controller=adminCiudad", "Links guardadas");
    }
}
